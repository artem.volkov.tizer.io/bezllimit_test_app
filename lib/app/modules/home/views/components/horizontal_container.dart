import 'package:flutter/material.dart';

class HorizontalContainer extends StatelessWidget {
  final double width;

  const HorizontalContainer({
    Key? key,
    this.width = 200,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 50,
      width: width,
      decoration: BoxDecoration(
        boxShadow: const [
          BoxShadow(
            color: Color.fromRGBO(0, 0, 0, 0.2),
            blurRadius: 10,
          ),
        ],
        color: Colors.white,
        borderRadius: BorderRadius.circular(50),
      ),
    );
  }
}
