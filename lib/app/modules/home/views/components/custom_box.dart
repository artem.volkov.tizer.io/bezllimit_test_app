import 'package:flutter/material.dart';

class CustomBox extends StatelessWidget {
  final Color color;
  final Function() onTap;

  const CustomBox({
    Key? key,
    this.color = Colors.white,
    required this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        decoration: BoxDecoration(
          color: color,
          borderRadius: BorderRadius.circular(15),
          border: Border.all(color: Colors.grey),
        ),
        height: 100,
        width: 100,
      ),
    );
  }
}
