import 'package:bezlimit_app/app/modules/home/controllers/home_controller.dart';
import 'package:bezlimit_app/app/modules/home/views/components/numeric_range_formatter.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class SelectBlockView extends GetView<HomeController> {
  const SelectBlockView();

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.black,
          title: Text('Second screen'),
          leading: GestureDetector(
            onTap: Get.back,
            child: Icon(Icons.arrow_back),
          ),
        ),
        body: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            TextFormField(
              textAlign: TextAlign.center,
              controller: controller.inputController,
              keyboardType: TextInputType.number,
              inputFormatters: [
                NumericalRangeFormatter(min: 0, max: controller.boxesAmount)
              ],
              autofocus: true,
              decoration: InputDecoration(
                  hintText:
                      "Enter a value between 0 and ${controller.boxesAmount - 1}"),
            ),
            Align(
              alignment: Alignment.bottomCenter,
              child: GestureDetector(
                onTap: () {
                  controller.saveBlocksNumber();
                  Get.back();
                  controller.scrollListToIndex();
                },
                child: Container(
                  height: 50,
                  width: 200,
                  decoration: BoxDecoration(
                    color: Colors.black,
                    borderRadius: BorderRadius.circular(25),
                  ),
                  child: Center(
                      child: Text(
                    'Save',
                    style: TextStyle(color: Colors.white, fontSize: 20),
                  )),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
