import 'dart:math' as math;

import 'package:bezlimit_app/app/modules/home/controllers/home_controller.dart';
import 'package:bezlimit_app/app/modules/home/views/components/custom_box.dart';
import 'package:bezlimit_app/app/modules/home/views/components/horizontal_container.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:scrollable_positioned_list/scrollable_positioned_list.dart';

class HomeView extends GetView<HomeController> {
  const HomeView();

  @override
  Widget build(BuildContext context) {
    var child;
    return Scaffold(
      body: SafeArea(
        child: LayoutBuilder(builder: (context, constraints) {
          final maxWidth = constraints.maxWidth;
          return Stack(
            children: <Widget>[
              Positioned(
                top: -maxWidth / 3.25,
                left: -maxWidth / 3.25,
                child: Obx(
                  () => Transform.rotate(
                    angle: math.pi / controller.extent,
                    child: RepaintBoundary(
                      child: SvgPicture.asset(
                        'assets/circle.svg',
                        height: maxWidth / 1.15,
                      ),
                    ),
                  ),
                ),
              ),
              NotificationListener<DraggableScrollableNotification>(
                onNotification: (notif) {
                  controller.extent = notif.extent;
                  if (notif.extent == 1) controller.showSnackBar(context);
                  return true;
                },
                child: DraggableScrollableSheet(
                    maxChildSize: 1,
                    minChildSize: 0.6,
                    initialChildSize: 0.6,
                    controller: controller.dragController,
                    builder: (context, dragController) {
                      child ??= SingleChildScrollView(
                          controller: dragController,
                          child: Container(
                            padding: EdgeInsets.fromLTRB(25, 25, 0, 25),
                            decoration: BoxDecoration(
                                color: Color.fromRGBO(220, 220, 220, 1),
                                borderRadius: BorderRadius.vertical(
                                    top: Radius.circular(15))),
                            height: constraints.maxHeight,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                HorizontalContainer(width: maxWidth - 100),
                                const SizedBox(height: 25),
                                HorizontalContainer(width: maxWidth - 50),
                                const SizedBox(height: 25),
                                HorizontalContainer(width: maxWidth - 150),
                                const SizedBox(height: 50),
                                RepaintBoundary(
                                  child: SizedBox(
                                    height: 100,
                                    child: ScrollablePositionedList.separated(
                                      scrollDirection: Axis.horizontal,
                                      itemCount: controller.boxesAmount,
                                      itemScrollController:
                                          controller.horizontalController,
                                      itemBuilder: (context, index) {
                                        return Obx(
                                          () => CustomBox(
                                            onTap: () => controller
                                                .openSelectBlockScreen(index),
                                            color: index.toString() ==
                                                    controller
                                                        .selectedBlockNumber
                                                ? Colors.red
                                                : Colors.white,
                                          ),
                                        );
                                      },
                                      separatorBuilder:
                                          (BuildContext context, int index) {
                                        return SizedBox(width: 20);
                                      },
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ));
                      return child;
                    }),
              ),
            ],
          );
        }),
      ),
    );
  }
}
