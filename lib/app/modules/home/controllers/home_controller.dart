import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:scrollable_positioned_list/scrollable_positioned_list.dart';

class HomeController extends GetxController {
  @override
  void onClose() {
    inputController.dispose();
    super.onClose();
  }

  final boxesAmount = 20;
  final _sheetExtent = 0.6.obs;
  final _selectedBlockNumber = ''.obs;

  final dragController = DraggableScrollableController();
  final inputController = TextEditingController();
  final horizontalController = ItemScrollController();

  double get extent => _sheetExtent.value;

  set extent(double extent) => _sheetExtent.value = extent;

  String get selectedBlockNumber => _selectedBlockNumber.value;

  set selectedBlockNumber(String extent) => _selectedBlockNumber.value = extent;

  void saveBlocksNumber() {
    if (inputController.value.text.isNotEmpty) {
      selectedBlockNumber = inputController.value.text;
    }
  }

  void showSnackBar(BuildContext context) {
    ScaffoldMessenger.of(context).showSnackBar(
      const SnackBar(
        content: Text('A SnackBar has been shown.'),
      ),
    );
  }

  void scrollListToIndex() {
    horizontalController.scrollTo(
      index: int.parse(selectedBlockNumber),
      duration: const Duration(milliseconds: 800),
      curve: Curves.easeInOut,
    );
  }

  void openSelectBlockScreen(int index) {
    Get.toNamed('/select-block');
    inputController.text = index.toString();
  }
}
