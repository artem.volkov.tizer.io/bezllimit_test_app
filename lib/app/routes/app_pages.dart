import 'package:get/get.dart';

import 'package:bezlimit_app/app/modules/home/bindings/home_binding.dart';
import 'package:bezlimit_app/app/modules/home/views/home_view.dart';
import 'package:bezlimit_app/app/modules/home/views/select_block_view.dart';

part 'app_routes.dart';

class AppPages {
  AppPages._();

  static const INITIAL = Routes.HOME;

  static final routes = [
    GetPage(
      name: _Paths.HOME,
      page: () => HomeView(),
      binding: HomeBinding(),
    ),
    GetPage(
      name: _Paths.SELECT_BLOCK,
      page: () => SelectBlockView(),
      binding: HomeBinding(),
    ),
  ];
}
